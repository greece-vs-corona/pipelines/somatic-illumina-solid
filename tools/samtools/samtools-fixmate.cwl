cwlVersion: v1.0
class: CommandLineTool

# This is only a partial/workflow-specific implementation for the 'fixmate' command

hints:
#- $import: envvar-global.yml
- $import: samtools-docker.yml
- class: InlineJavascriptRequirement

inputs:

  input:
    type: File
    inputBinding:
      position: 4

#  threads:
#    type: int?
#    inputBinding:
#      position: 1
#      prefix: -@

  output_format:
    type: string
    inputBinding:
      position: 2
      prefix: -O

  output_name:
    type: string
    inputBinding:
      position: 5


outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)


baseCommand: [samtools, fixmate]
