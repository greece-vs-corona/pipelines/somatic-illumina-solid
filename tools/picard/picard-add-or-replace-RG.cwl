cwlVersion: v1.0
class: CommandLineTool

hints:
#- $import: envvar-global.yml
- $import: picard-docker.yml
- class: InlineJavascriptRequirement

inputs:
  java_arg:
    type: string
    default: "-Xmx2g"
    inputBinding:
      position: 1

  input:
    type: File
    inputBinding:
      position: 4
      prefix: "INPUT="
    doc: |
      The BAM or SAM file to sort. Required

  output_name:
    type: string
    inputBinding:
      position: 5
      prefix: "OUTPUT="
    doc: |
      The sorted BAM or SAM output file. Required

  rgid:
    type: string
    inputBinding:
      position: 6
      prefix: "RGID="

  rglb:
    type: string
    inputBinding:
      position: 7
      prefix: "RGLB="

  rgpl:
    type: string
    inputBinding:
      position: 8
      prefix: "RGPL="

  rgpu:
    type: string
    inputBinding:
      position: 9
      prefix: "RGPU="

  rgsm:
    type: string
    inputBinding:
      position: 10
      prefix: "RGSM="

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

  index:
    type: File
    outputBinding:
      glob: $(inputs.output_name.split('.')[0].concat(".bai"))

arguments:

- valueFrom: "/usr/local/bin/picard.jar"
  position: 2
  prefix: "-jar"

- valueFrom: "AddOrReplaceReadGroups"
  position: 3

- valueFrom: "true"
  position: 11
  separate: false
  prefix: CREATE_INDEX=

baseCommand: ["java"]
