cwlVersion: v1.0
class: CommandLineTool

# This is only a partial/workflow-specific implementation for the 'bedtools' command

hints:
  #- $import: envvar-global.yml
  - $import: bedtools-docker.yml
  - class: InlineJavascriptRequirement

inputs:

  A-file:
    type: File
    inputBinding:
      position: 1
      prefix: -a
    doc: |
      BAM/BED/GFF/VCF file “A”. Each feature in A is compared to B in search of overlaps.;
      Use “stdin” if passing A with a UNIX pipe.

  B-file:
    type: File
    inputBinding:
      position: 2
      prefix: -b
    doc: |
      One or more BAM/BED/GFF/VCF file(s) “B”. Use “stdin” if passing B with a UNIX pipe.;
      NEW!!!: -b may be followed with multiple databases and/or wildcard (*) character(s).

  output_name:
    type: string


stdout: $(inputs.output_name)

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

arguments:

  - valueFrom: "-mean"
    position: 3


baseCommand: ["bedtools", "coverage"]
