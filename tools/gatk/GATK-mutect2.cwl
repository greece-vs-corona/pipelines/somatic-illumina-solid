cwlVersion: v1.0
class: CommandLineTool

# This is only a partial/workflow-specific implementation for the 'mutect2' command

hints:
#- $import: envvar-global.yml
- $import: GATK-docker.yml
- class: InlineJavascriptRequirement

inputs:

  reference:
    type: File
    secondaryFiles:
      - .amb
      - .ann
      - .bwt
      - .pac
      - .sa
      - .fai
      - ^.dict
    inputBinding:
        position: 3
        separate: true
        prefix: --reference

  input:
    type: File
    secondaryFiles:
      - ^.bai
    inputBinding:
      position: 4
      separate: true
      prefix: --input

  tumor_sample:
    type: string
    inputBinding:
      position: 5
      separate: true
      prefix: --tumor-sample

  output_name:
    type: string
    inputBinding:
      position: 6
      prefix: --output
    doc: name of the output file from Mutect2

outputs:
  output:
    type: File
    outputBinding:
      glob: $(inputs.output_name)

arguments:

- valueFrom: "/gatk/gatk.jar"
  position: 1
  separate: true
  prefix: "-jar"

- valueFrom: "Mutect2"
  position: 2
  # separate: true
  # prefix: "-T"

baseCommand: ["java"]
