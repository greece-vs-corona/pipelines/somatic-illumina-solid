FROM r-base:3.6.1

# Install C/C++ libraries
RUN apt-get update \
  && apt-get install -y \
    libssl-dev \
    libxml2-dev \
    libcurl4-openssl-dev \
    gcc-9-base \
    libgcc-9-dev \
    libc6-dev
    # libcurl4-gnutls-dev

COPY scripts /usr/local/src/scripts
WORKDIR /usr/local/src/scripts

# Install the R libraries needed to run the scripts
RUN /usr/bin/R --vanilla -f ./install_libs.R
# Install BSgenome hg19.masked now in order to be stored as cached in the docker image 
# and not pull it everytime the script runs
RUN /usr/bin/R --vanilla -f ./install_bsgenome.R

# Execute the target script
ENTRYPOINT ["Rscript", "/usr/local/src/scripts/runCoverageReporter.R"]
CMD [""]
