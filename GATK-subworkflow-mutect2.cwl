cwlVersion: v1.0
class: Workflow

requirements:
  - class: StepInputExpressionRequirement
  - class: InlineJavascriptRequirement

inputs:

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # bwa-mem
  reference:
    type: File
    doc: reference human genome file

  reads:
    type: File[]
    doc: files containing the paired end reads in fastq format

  bwa_mem_output:
    type: string

  bwa_mem_threads:
    type: int

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools
  samtools_threads:
    type: int

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-view-1
  samtools_view_1_isbam:
    type: boolean
    doc: boolean set to output bam file from samtools view

  samtools_view_1_output:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-fixmate
  samtools_fixmate_output:
    type: string

  samtools_fixmate_output_format:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-sort
  samtools_sort_output:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-view-2
  samtools_view_2_isbam:
    type: boolean
    doc: boolean set to output bam file from samtools view

  samtools_view_2_output:
    type: string

  samtools_samheader:
    type: boolean

  samtools_readswithoutbits:
    type: int

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # # samtools-index
  # samtools_index_bai:
  #  type: boolean
  #  doc: boolean set to output bam file from samtools view

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools_flagstat
  samtools_flagstat_output:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-view-3
  samtools_view_3_output:
    type: string

  target:
    type: File

  samtools_view_3_count:
    type: boolean

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # samtools-view-4
  samtools_view_4_output:
    type: string

  samtools_view_4_count:
    type: boolean

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # picard-add-or-replace-RG
  picard_add_or_replace_RG_output:
    type: string

  rgid:
    type: string

  rglb:
    type: string

  rgpl:
    type: string

  rgpu:
    type: string

  rgsm:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # GATK-mutect2
  GATK_mutect2_output:
    type: string

  GATK_mutect2_tumor_sample:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # bedtools-coverage
  bedtools_coverage_output:
    type: string

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  # coverage-reporter
  coverage-reporter-org:
    type: string

  coverage-reporter-dpcalc:
    type: string

  coverage-reporter-fraglen:
    type: int

  coverage-reporter-fragfac:
    type: int

  coverage-reporter-output_filename:
    type: string

  coverage-reporter-indexoutput:
    type: boolean

  coverage-reporter-corefraction:
    type: float
  #~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

outputs:
  bwa_mem_out:
    type: File
    outputSource: bwa-mem/output

  samtools_view_1_out:
    type: File
    outputSource: samtools-view-1/output

  samtools_fixmate_out:
    type: File
    outputSource: samtools-fixmate/output

  samtools_sort_out:
    type: File
    outputSource: samtools-sort/sorted

  samtools_view_2_out:
    type: File
    outputSource: samtools-view-2/output

  picard_add_or_replace_RG_out:
    type: File
    outputSource: picard-add-or-replace-RG/output

  picard_add_or_replace_RG_index_out:
    type: File
    outputSource: picard-add-or-replace-RG/index


  # samtools_index_out:
  #   type: File
  #   outputSource: samtools-index/index

  samtools_flagstat_out:
    type: File
    outputSource: samtools-flagstat/output

  bedtools-coverage:
    type: File
    outputSource: bedtools-coverage/output

  samtools_view_3_out:
    type: File
    outputSource: samtools-view-3/output

  samtools_view_4_out:
    type: File
    outputSource: samtools-view-4/output

  GATK_mutect2_out:
    type: File
    outputSource: GATK-mutect2/output

  coverage-reporter_out:
    type: File
    outputSource: coverage-reporter/output



steps:

  # bwa mem -M -t 16  /path/to/reference/genome/fa  input_R1.fastq.gz input_R2_001.fastq.gz > output.sam
  bwa-mem:
    run: ./tools/bwa/bwa-mem.cwl
    in:
      reference: reference
      reads: reads
      output_filename: bwa_mem_output
      threads: bwa_mem_threads
    out: [output]


  # samtools view -@ 16 -Sb output.sam > output.bam
  samtools-view-1:
    run: ./tools/samtools/samtools-view.cwl
    in:
      input: bwa-mem/output
      isbam: samtools_view_1_isbam
      # sambam?
      output_name: samtools_view_1_output
      threads: samtools_threads

    out: [output]


  # samtools fixmate -@ 16 -O bam output.bam output.fixed.bam
  samtools-fixmate:
    run: ./tools/samtools/samtools-fixmate.cwl
    in:
      input: samtools-view-1/output
      output_name: samtools_fixmate_output
      threads: samtools_threads
      output_format: samtools_fixmate_output_format

    out: [output]


  # samtools sort -@ 16 -o output.fixed.sorted.bam output.fixed.bam
  samtools-sort:
    run: ./tools/samtools/samtools-sort.cwl
    in:
      input: samtools-fixmate/output
      output_name: samtools_sort_output
      threads: samtools_threads

    out: [sorted]


  # samtools view -@ 16 -h -F 0x904 -b output.fixed.sorted.bam > output.fixed.sorted.uniq.bam
  samtools-view-2:
    run: ./tools/samtools/samtools-view.cwl
    in:
      input: samtools-sort/sorted
      isbam: samtools_view_2_isbam
      output_name: samtools_view_2_output
      samheader: samtools_samheader
      readswithoutbits: samtools_readswithoutbits
      threads: samtools_threads

    out: [output]


  # java -jar picard.jar AddOrReplaceReadGroups I=output.fixed.sorted.uniq.bam O=output.fixed.sorted.uniq.rg.bam RGID=1 RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=output
  picard-add-or-replace-RG:
    run: ./tools/picard/picard-add-or-replace-RG.cwl
    in:
      input: samtools-view-2/output
      output_name: picard_add_or_replace_RG_output
      rgid: rgid
      rglb: rglb
      rgpl: rgpl
      rgpu: rgpu
      rgsm: rgsm

    out: [output, index]


  # # Using previous step's 'picard' for creating an index
  # # samtools index output.fixed.sorted.uniq.rg.bam
  # samtools-index:
  #   run: ./tools/samtools/samtools-index.cwl
  #   in:
  #     input: picard-add-or-replace-RG/output
  #     # bai: samtools_index_bai     # default is true
  #
  #   out: [index]


  # samtools flagstat -@ 16 output.fixed.sorted.uniq.rg.bam > output.align.stats.txt
  samtools-flagstat:
    run: ./tools/samtools/samtools-flagstat.cwl
    in:
      input: picard-add-or-replace-RG/output
      output_name: samtools_flagstat_output
      threads: samtools_threads

    out: [output]


  #bedtools coverage -a trusight-myeloid-amplicon-track.bed -b output.fixed.sorted.uniq.rg.bam -mean > output.coverage.mean.txt
  bedtools-coverage:
    run: ./tools/bedtools/bedtools-coverage.cwl
    in:
      A-file: target
      B-file: picard-add-or-replace-RG/output
      output_name: bedtools_coverage_output

    out: [output]

  # samtools view s-L trusight-myeloid-amplicon-track.bed -c output.fixed.sorted.uniq.rg.bam > output.count.ontarget.txt
  samtools-view-3:
    run: ./tools/samtools/samtools-view.cwl
    in:
      input: picard-add-or-replace-RG/output
      output_name: samtools_view_3_output
      bedoverlap: target
      count: samtools_view_3_count

    out: [output]


  # samtools view -c output.fixed.sorted.uniq.rg.bam > output.count.total.txt
  samtools-view-4:
    run: ./tools/samtools/samtools-view.cwl
    in:
      input: picard-add-or-replace-RG/output
      output_name: samtools_view_4_output
      count: samtools_view_4_count

    out: [output]


  # gatk Mutect2 --reference /path/to/reference/genome/fa --input <output.fixed.sorted.uniq.rg.bam
  # --tumor-sample output --output output_GATK_variants.vcf.gz  > output.Mutect2_4_1_0_0.out 2>&1
  GATK-mutect2:
    run: ./tools/gatk/GATK-mutect2.cwl
    in:
      input: picard-add-or-replace-RG/output
      tumor_sample: GATK_mutect2_tumor_sample
      reference: reference
      output_name: GATK_mutect2_output

    out: [output]


  coverage-reporter:
    run: ./tools/custom/gatk-coverage-reporter/gatk-coverage-reporter.cwl
    in:
      bam: picard-add-or-replace-RG/output
      vcf: GATK-mutect2/output
      org: coverage-reporter-org
      dpcalc: coverage-reporter-dpcalc
      fraglen: coverage-reporter-fraglen
      fragfac: coverage-reporter-fragfac
      output_filename: coverage-reporter-output_filename
      indexoutput: coverage-reporter-indexoutput
      corefraction: coverage-reporter-corefraction

    out: [output]
